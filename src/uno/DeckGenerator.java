/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package uno;


public class DeckGenerator {
    
    //method to generate a Deck of 36 cards with numbers 1-9 for each color: red, green, blue, yellow
    public void generateDeck(){
        
        for(int i=0; i<9; i++){
            Card card = new Card("yellow", ++i);
            Card [] newDeck = new Card[36];
            newDeck[i] = card;
        }
        
    
    }
    
}
