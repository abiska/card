/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno;

/**
 * A class that models playing card Objects. Cards have 
 * a value (note that Ace = 1, Jack -11, Queen =12, King = 13)
 * A suit (clubs, hearts, spades, diamonds).
 * There are 52 cards in a deck, no jokers.
 * This code is to be used in ICE1. When you create your own branch,
 * add your name as a modifier.
 * @author dancye
 */
public class Card {
   
   //attributes
   private String color; //red, green, blue, yellow
   private int number; //1-9

   //contructors
    public Card() {}
    public Card(String color, int number) {
        this.color = color;
        this.number = number;
    }
   
   
   //getters
    public String getColor() {
        return color;
    }
    public int getNumber() {
        return number;
    }

   
}
