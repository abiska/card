/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno;

/**
 * Simulation Class.
 * @author iuabd
 */
public class CardTrick {
    
    public static void main(String[] args)
    {   
        
        Card card1 = new Card("red", 1); //experimental
        Card card2 = new Card("yellow", 1); //experimental
        
        //generate a deck of cards
        DeckGenerator deckGen = new DeckGenerator();
        deckGen.generateDeck();
        
        //create two hands
        Hand hand1 = new Hand();
        Hand hand2 = new Hand();
                
        //create two players
        Player player1 = new Player("Tom", hand1);
        Player player2 = new Player("Jerry", hand2);
        
        System.out.println( "The Game is starting in 3, 2, 1... Now!" );
        
        System.out.println( "\nIt's your turn " + player1.getId() + "!" );
        hand1.playCard(card1);
        System.out.println( "Card [" + card1.getColor()+"-"+card1.getNumber()+"] " + "is played by " + player1.getId() );
        
        System.out.println( "\nIt's your turn " + player2.getId() + "!" );
        hand2.playCard(card2);
        System.out.println( "Card [" + card2.getColor()+"-"+card2.getNumber()+"] " + "is played by " + player2.getId() );
        
        System.out.println("\nGame over! It's a draw?");
    }
    
}
