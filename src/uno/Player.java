/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package uno;

/**
 *
 * @author iuabd
 */
public class Player {
    
    //attributes
    private String id;
    private Hand hand;
    
    //getters
    public String getId() {
        return id;
    }
    public Hand getHand() {
        return hand;
    }
    
    //constructor

    public Player(String id, Hand hand) {
        this.id = id;
        this.hand = hand;
    }
    
}
